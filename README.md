# Lucha MEdieval

Este proyecto simula una lucha entre elfos, humanos y los orcos.

### Pre-requisitos 📋

Para ejecutar este proyecto en tu ordendor debes tener intalado la herrmienta SpringToolSuite.
Puedes descargarlo en el siguiente enlace 👉 [SpringToolSuite](https://spring.io/tools)

### Instalación 🔧

Los pasos para importar el proyecto en SpringToolSuite son los siguientes:
1. En el menú, barra superior, le das a **File > Import**
2. Se te abrirá una ventana y le das a la opción **Git > Projects from Git**. Luego pulsas el botón siguiente
3. A continuación, se te abre una ventana. En esta ventana eliges la opción **clone URI**
4. En la siguiente ventana, en el campo URI, introduces la siguiente URI: https://gitlab.com/jmetene/LuchaMedieval.git y pulsas **Finish**

## Autores ✒️

* **Juan Crisóbal Metene** [jmetene](https://gitlab.com/jmeten)

## Licencia 📄

Este proyecto está bajo la Licencia (Sopra Steria 😎) - mira el archivo [LICENSE.md](https://gitlab.com/jmetene/LuchaMedieval/-/blob/main/LICENSE) para detalles


---
⌨️ con ❤️ por [jmetene](https://gitlab.com/jmetene) 😊
